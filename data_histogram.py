import seaborn as sns
from matplotlib import pyplot as plt

from train_data import TrainDataLoader
import os

train_data_loader = TrainDataLoader(os.path.abspath("./data"))
_, labels = train_data_loader.get_train_data()

print(labels.shape)

plt.hist(labels, bins=43, rwidth=0.5)
plt.title("Traffic signs frequency graph")
plt.xlim(0, 43)
plt.xlabel("ClassId")
plt.ylabel("Frequency")
plt.show()
