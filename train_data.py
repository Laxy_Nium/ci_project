import os
from typing import Tuple, List

import numpy as np
from PIL import Image
import pickle

from numpy import ndarray


class TrainDataLoader:
    def __init__(self, dataPath):
        self.dataPath = dataPath
        self.trainPath = os.path.join(self.dataPath, 'Train')
        self.CATEGORIES_COUNT = self.get_categories_count()
        self.HEIGHT = 32
        self.WIDTH = 32

    def get_train_data(self) -> Tuple[ndarray, ndarray]:
        pickle_path = os.path.join(self.dataPath, 'train_data.pickle')
        if os.path.exists(pickle_path):
            with open(pickle_path, "rb") as fh:
                return pickle.load(fh)
        data = self.prepare_data_and_labels()
        with open(pickle_path, 'wb') as fh:
            pickle.dump(data, fh)
        return data

    def prepare_data_and_labels(self):
        height = self.HEIGHT
        width = self.WIDTH
        categories_count = self.CATEGORIES_COUNT
        data = []
        labels = []

        for i in range(categories_count):
            path = os.path.join(self.trainPath, str(i))
            images = os.listdir(path)

            for a in images:
                image = Image.open(path + '/' + a)
                image = image.resize((height, width))
                image = np.array(image)
                data.append(image)
                labels.append(i)

        data = np.array(data)
        labels = np.array(labels)
        return data, labels

    def get_categories_count(self):
        categories_count = len(os.listdir(self.trainPath))
        return categories_count
