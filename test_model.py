import os

import keras.models
from tensorflow import keras
import numpy as np
from sklearn.metrics import accuracy_score

from test_data import TestDataLoader


def run():
    data_dir = os.path.abspath("./data")

    test_data_loader = TestDataLoader(data_dir)

    data, labels = test_data_loader.get_test_data()

    x_pred = np.array(data)
    x_pred = x_pred / 255

    # Predicting
    final_modal_path = 'final_model.hdf5'

    model = keras.models.load_model(final_modal_path)

    pred = model.predict_classes(x_pred)

    print('Test Data accuracy: ', accuracy_score(labels, pred) * 100)
    y_pred = keras.utils.to_categorical(labels, 43)
    model.evaluate(x_pred, y_pred)


if __name__ == "__main__":
    run()
