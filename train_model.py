import os
import pickle

import tensorflow as tf
import tensorflow.python.keras.backend as K
from keras_preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split
from tensorflow import keras
from tensorflow.python.keras import Sequential
from tensorflow.python.keras.callbacks import ModelCheckpoint
from tensorflow.python.keras.layers import Dropout, Flatten, Dense, Conv2D, MaxPool2D
import models

from train_data import TrainDataLoader

print("Num GPUs Available: ", len(tf.config.list_physical_devices('GPU')))
tf.debugging.set_log_device_placement(True)
K._get_available_gpus()


def run():
    # Data needs to be firstly downloaded from https://www.kaggle.com/meowmeowmeowmeowmeow/gtsrb-german-traffic-sign
    data_dir = os.path.abspath("./data")
    train_path = os.path.abspath("./data/Train")

    train_data_loader = TrainDataLoader(data_dir)

    data, labels = train_data_loader.get_train_data()

    print(data.shape, labels.shape)

    X_train, X_test, Y_train, Y_test = train_test_split(data, labels, test_size=0.2, random_state=42, shuffle=True)

    X_train = X_train / 255
    X_test = X_test / 255

    print(X_train.shape, X_test.shape, Y_train.shape, Y_test.shape)

    Y_train = keras.utils.to_categorical(Y_train, train_data_loader.CATEGORIES_COUNT)
    Y_test = keras.utils.to_categorical(Y_test, train_data_loader.CATEGORIES_COUNT)

    print(Y_train.shape)
    print(Y_test.shape)

    model = models.build_best_model()
    model.summary()

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    aug = ImageDataGenerator(
        rotation_range=10,
        zoom_range=0.15,
        width_shift_range=0.1,
        height_shift_range=0.1,
        shear_range=0.15,
        horizontal_flip=False,
        vertical_flip=False,
        fill_mode="nearest")

    batch_size = 32
    epochs = 15

    filepath = "saved_model-{epoch:02d}-{val_loss:.2f}.hdf5"
    checkpoint = ModelCheckpoint(filepath, monitor='val_accuracy', mode='auto', save_best_only=False,
                                 save_weights_only=False)

    history = model.fit(aug.flow(X_train, Y_train, batch_size=batch_size),
                        epochs=epochs, validation_data=(X_test, Y_test), callbacks=[checkpoint])

    final_modal_path = 'final_model.hdf5'
    model.save(final_modal_path)

    with open('final_model_history.pickle', 'wb') as fh:
        pickle.dump(history.history, fh)


if __name__ == "__main__":
    run()
