import os

from PIL import Image
from matplotlib import pyplot as plt

meta_files_path = os.path.abspath("./data/Meta")

plt.figure(figsize=(16, 16))
plt.tight_layout()
plt.subplots_adjust(wspace=0.2)
plt.subplots_adjust(hspace=0.0001)
for i in range(0, 43):
    file_path = os.path.join(meta_files_path, str(i) + ".png")
    print(file_path)
    img = Image.open(file_path)
    x = plt.subplot(4, 11, i + 1)
    x.title.set_text(i)
    plt.axis('off')
    plt.imshow(img)

plt.show()
