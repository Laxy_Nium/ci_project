import os
from typing import Tuple, List

import numpy as np
import pandas as pd
from PIL import Image
import pickle

from numpy import ndarray


class TestDataLoader:
    def __init__(self, dataPath):
        self.dataPath = dataPath
        self.testPath = os.path.join(self.dataPath, 'Train')
        self.HEIGHT = 32
        self.WIDTH = 32

    def get_test_data(self) -> Tuple[List[ndarray], ndarray]:
        pickle_path = os.path.join(self.dataPath, 'test_data.pickle')
        if os.path.exists(pickle_path):
            with open(pickle_path, "rb") as fh:
                return pickle.load(fh)
        data = self.prepare_data_and_labels()
        with open(pickle_path, 'wb') as fh:
            pickle.dump(data, fh)
        return data

    def prepare_data_and_labels(self):
        height = self.HEIGHT
        width = self.WIDTH

        test = pd.read_csv(os.path.join(self.dataPath, 'Test.csv'))
        labels = test["ClassId"].values
        images = test["Path"].values

        data = []

        for a in images:
            image = Image.open(self.dataPath + '/' + a)
            image = image.resize((height, width))
            image = np.array(image)
            data.append(image)

        return data, labels
