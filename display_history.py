import pickle

from matplotlib import pyplot as plt

with open('final_model_history.pickle', 'rb') as file:
    history = pickle.load(file)

plt.figure(0)
plt.plot(history['accuracy'], label='training accuracy')
plt.plot(history['val_accuracy'], label='val accuracy')
plt.title('Accuracy')
plt.xlabel('epochs')
plt.ylabel('accuracy')
plt.legend()
plt.show()

plt.figure(1)
plt.plot(history['loss'], label='training loss')
plt.plot(history['val_loss'], label='val loss')
plt.title('Loss')
plt.xlabel('epochs')
plt.ylabel('loss')
plt.legend()
plt.show()
